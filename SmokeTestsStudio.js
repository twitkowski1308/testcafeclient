import { Selector, ClientFunction } from 'testcafe';

fixture `New Fixture`
    .page `http://release.nfj.io`
    .httpAuth({
        username: 'dev',
        password: 'beware007devs'
    });

test('ST_001Wyświetlanie_Kategorii', async t => {
await t
        .maximizeWindow( )
    //1.Sprawdzenie kategorii Backend jobs
        .expect(Selector('.category-e2e.ng-binding').nth(0).textContent).eql("Backend jobs");
    //2.Sprawdzenie kategorii Full Stack job 
await t
        .expect(Selector('.category-e2e.ng-binding').nth(1).textContent).eql("Full Stack jobs")
    //3.Sprawdzenie kategorii Mobile Embedded jobs 
        .expect(Selector('.category-e2e.ng-binding').nth(2).textContent).eql("Mobile & Embedded jobs")
    //4.Sprawdzenie kategorii Frontend jobs  
        .expect(Selector('.category-e2e.ng-binding').nth(3).textContent).eql("Frontend jobs")
    //5.Sprawdzenie kategorii Testing jobs 
        .expect(Selector('.category-e2e.ng-binding').nth(4).textContent).eql("Testing jobs")
    //6.Sprawdzenie kategorii DevOps jobs 
        .expect(Selector('.category-e2e.ng-binding').nth(5).textContent).eql("DevOps jobs")
    //7.Sprawdzenie kategorii Business Intelligence jobs
        .expect(Selector('.category-e2e.ng-binding').nth(6).textContent).eql("Business Intelligence jobs")
    //8.Sprawdzenie kategorii HR jobs
        .expect(Selector('.category-e2e.ng-binding').nth(7).textContent).eql("HR jobs")
    //9.Sprawdzenie kategorii IT Trainee jobs
        .expect(Selector('.category-e2e.ng-binding').nth(8).textContent).eql("IT Trainee jobs")
    //10.Sprawdzenie kategorii UX Designer jobs 
        .expect(Selector('.category-e2e.ng-binding').nth(9).textContent).eql("UX Designer jobs")
    //11.Sprawdzenie kategorii Support jobs 
        .expect(Selector('.category-e2e.ng-binding').nth(10).textContent).eql("Support jobs")
    //12.Sprawdzenie kategorii Project Manager jobs  
        .expect(Selector('.category-e2e.ng-binding').nth(11).textContent).eql("Project Manager jobs")
    //13.Sprawdzenie kategorii Business Analyst jobs
        .expect(Selector('.category-e2e.ng-binding').nth(12).textContent).eql("Business Analyst jobs")
    //14.Sprawdzenie kategorii Other jobs 
        .expect(Selector('.category-e2e.ng-binding').nth(13).textContent).eql("Other jobs")
    //"15.Włączenie mapki"
        .click(Selector('.btn-map').find('img')) 
    //16.Sprawdzenie widoczności mapki"
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(true)
    //17.Wyłączenie mapki"
        .click(Selector('button').withText('CLOSE'))
    //18.Sprawdzenie widoczności mapki"
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).notEql(true);
});

test('ST_002Wyświetlanie_listy_firm', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('Companies'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('0'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('#content-list').find('span').withText('A'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('B'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(3).find('span').withText('C'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('D'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('span').withText('E'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('#content-list').find('span').withText('F'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount ).gt(0)
        .click(Selector('#content-list').find('span').withText('G'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount ).gt(0)
        .click(Selector('#content-list').find('span').withText('H'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount ).gte(0)
        .click(Selector('#content-list').find('span').withText('I'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('J'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('K'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(12).find('span').withText('L'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('M'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('N'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(15).find('span').withText('O'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(16).find('span').withText('P'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('span').withText('Q'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('R'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('S'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(20).find('span').withText('T'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('U'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('span').withText('V'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(23).find('span').withText('W'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('X'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('span').withText('Y'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('span').withText('Z'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('span').withText('Z'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('[class^="toggle-switch ng-pristine ng-untouched ng-valid ng"]'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').find('div').find('div').find('input'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('body').find('div').nth(2).find('header').find('div').nth(6).find('search-header').find('div').find('img'))
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(true)
        .click(Selector('span').withText('CLOSE'))
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(false);
});

test('ST_003Wyświetlanie_ogłoszenia', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('.job-offer-title').nth(0))
        .expect(Selector('.ng-binding').nth(1).visible).eql(true)
        .expect(Selector('.link-disabled').find('a').visible).eql(true)
        .expect(Selector('.article-header-navigation').find('link-next').find('a').visible).eql(true)
        .expect(Selector('.panel.border-top.border-top1.ng-isolate-scope').find('div').withText('ESSENTIALS').visible).eql(true)
        .expect(Selector('.panel.border-top.border-top2').find('div').withText('SPECS').visible).eql(true)
        .expect(Selector('[class^="panel margin-bottom-10 border-top ng-isolate-scope"]').find('div').withText('PROJECT').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10.ng-isolate-scope').nth(1).find('div').withText('ONE').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10').nth(2).find('div').withText('JOB PROFILE').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10').nth(3).find('div').withText('EQUIPMENT SUPPLIED').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10').nth(4).find('div').withText('TECHNOLOGIES USED').visible).eql(true)
        .expect(Selector('.panel').nth(7).find('div').withText('OTHER INFO').visible).eql(true)
        .expect(Selector('[class^="panel border-top this-and-that ng-isolate-scope bo"]').find('div').withText('THIS AND THAT').visible).eql(true)
        .expect(Selector('[class^="panel border-top border-top3 margin-bottom-10 ng-i"]').find('div').withText('REQUIREMENTS').visible).eql(true)
        .expect(Selector('.panel-heading').visible).eql(true)
        .expect(Selector('h2').withText('SEE ALSO SIMILAR ADS').visible).eql(true)
        .click(Selector('.btn-map').find('img'))
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(true)
        .click(Selector('span').withText('CLOSE'))
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(false);
});

test('ST_004Wyświetlanie_profilu_firmy', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('Companies'))
        .click(Selector('.company-item.pin-animate').nth(0))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').find('div').find('div').find('company-component').find('div').find('article-header').find('div').nth(4).find('div').find('div').find('div').find('div').find('div').find('div').find('h1').visible).eql(true)
        .expect(Selector('h1').withText('About company').visible).eql(true)
        .expect(Selector('h1').withText('Technologies').visible).eql(true)
        .expect(Selector('h1').withText('Benefits').visible).eql(true)
        .expect(Selector('h1').withText('Job Offers').visible).eql(true)
        .expect(Selector('#company-location').visible).eql(true)
        .click(Selector('.btn-map.ng-scope').find('img'))
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(true)
        .click(Selector('span').withText('CLOSE'))
        .expect(Selector('.gm-style').find('div').find('div').nth(76).visible).eql(false);
});

test('ST_005Porównywanie_ofert', async t => {
    await t
        .maximizeWindow( )
        .hover(Selector('.list-item.pin-animate').nth(0))
        .click(Selector('.glyphicon.glyphicon-plus').nth(0))
        .hover(Selector('.list-item.pin-animate').nth(1))
        .click(Selector('.glyphicon.glyphicon-plus').nth(1))
        .hover(Selector('.list-item.pin-animate').nth(2))
        .click(Selector('.glyphicon.glyphicon-plus').nth(2))
        .hover(Selector('.panel-body').nth(1).nth(1).find('div').nth(1))
        .click(Selector('.glyphicon.glyphicon-shopping-cart'))
        .expect(Selector('h2').withText('TITLE').visible).eql(true)
        .expect(Selector('h2').withText('CATEGORY').visible).eql(true)
        .expect(Selector('h2').withText('ESSENTIALS').visible).eql(true)
        .expect(Selector('h2').withText('SPECS').visible).eql(true)
        .expect(Selector('h2').withText('WORK METHOD').visible).eql(true)
        .expect(Selector('h2').withText('ONE').visible).eql(true)
        .expect(Selector('h2').withText('JOB PROFILE').visible).eql(true)
        .expect(Selector('h2').withText('EQUIPMENT').visible).eql(true)
        .expect(Selector('h2').withText('TECHS').visible).eql(true)
        .expect(Selector('h2').withText('OTHER INFO').visible).eql(true)
        .expect(Selector('h2').withText('THIS AND THAT').visible).eql(true)
        .expect(Selector('h2').withText('MUST').visible).eql(true)
        .expect(Selector('h2').withText('NICE').visible).eql(true)
        .expect(Selector('h2').withText('LANGUAGES').visible).eql(true)
        .expect(Selector('h2').withText('OTHER REQS').visible).eql(true)
        .expect(Selector('.posting-column.ng-scope').nth(2).exists).eql(true)
        .hover(Selector('.compare-widget-items.ng-scope'))
        .click(Selector('[class^="glyphicon glyphicon-remove compare-widget-remove n"]').nth(1))
        .expect(Selector('.posting-column.ng-scope').nth(2).exists).eql(false);
});

test('ST_006Przyciski_Social Media-ogłoszenie', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    const targetTitle = ('Test Additional Maila Engineer')
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('Test Additional Maila Engineer'))
        .expect(Selector('#content-list').find('.btn.btn-circle.btn-info.ng-scope.ng-isolate-scope').exists).eql(true)
        .expect(Selector('#content-list').find('.twitter-image.twitter-image-md').exists).eql(true)
        .expect(Selector('div').withText('in').nth(71).find('div').nth(2).exists).eql(true)
        .expect(Selector('button').withText('in').exists).eql(true)
        .expect(Selector('.panel-body.social-buttons').nth(1).find('[class^="btn btn-image btn-feedback btn-feedback-md ng-isol"]').exists).eql(true)
        .expect(Selector('.btn.btn-circle.btn-info.btn-pdf.text-bold').find('div').withText('PDF').exists).eql(true)
        .click(Selector('.glyphicon.glyphicon-heart-empty'))
        .expect(Selector('.panel.nfj-modal').find('div').withText('f').exists).eql(true)
        .click(Selector('[class^="modal nfj-social-login-modal fade ng-scope ng-isol"]'))
        .click(Selector('.twitter-image.twitter-image-md'))
        .expect(Selector('#header').find('a').withText('Twitter').textContent).eql("Twitter")
        .expect(Selector('#status').textContent).contains(targetTitle)
        .navigateTo('http://release.nfj.io')
        .click(Selector('span').withText('Test Additional Maila Engineer'))
        .click(Selector('div').withText('in').nth(71).find('div').nth(2).find('a').find('img'))
        .expect(Selector('#homelink').textContent).eql("Facebook")
        .navigateTo('http://release.nfj.io')
        .click(Selector('span').withText('Test Additional Maila Engineer'))
        .click(Selector('button').withText('in'))
        .switchToIframe(Selector('.authentication-iframe'))
        .expect(Selector('h1').textContent).eql("LinkedIn")
        .switchToMainWindow()
        .navigateTo('http://release.nfj.io')
        .click(Selector('span').withText('Test Additional Maila Engineer'))
        .click(Selector('[class^="btn btn-image btn-feedback btn-feedback-md ng-isol"]'))
        .expect(Selector('h1').withText('Share posting with a friend').textContent).eql("Share posting with a friend")
        .expect(Selector('.form-group').nth(2).nth(2).find('input').value).contains(targetTitle)
        .click(Selector('button').withText('Cancel'))
        .click(Selector('.btn.btn-circle.btn-info.btn-pdf.text-bold').find('div').withText('PDF'))
        await t
        .expect(getLocation()).contains('/pdf/?id=');
});

test('ST_007Przyciski_Social_Media-lista ogłoszeń', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    await t
        .maximizeWindow( )
        .click(Selector('.twitter-image.twitter-image-md'))
        .expect(Selector('#status').textContent).contains("#NoFluffJobs: innovative #IT job board with challenging #ITjobs and companies that have nothing to hide #recruitment\nhttp://nofluffjobs.com")
        .navigateTo('http://release.nfj.io')
        .click(Selector('.panel-body.social-buttons').find('div').nth(2).find('a').find('img'))
        .expect(Selector('#homelink').textContent).eql("Facebook")
        .navigateTo('http://release.nfj.io')
        .click(Selector('button').withText('in'))
        .switchToIframe(Selector('.authentication-iframe'))
        .expect(Selector('h1').textContent).eql("LinkedIn")        
        .switchToMainWindow()
        .navigateTo('http://release.nfj.io')
        //.click(Selector('#container').find('[alt="RSS"]'))        
        //.expect(getLocation()).contains('/rss')
});

test('ST_008Poprawność_Linków_Ogłoszeń', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    var title = Selector('.ng-binding').nth(1).textContent
    const company = Selector('.ng-binding').nth(2).textContent
    await t
        .maximizeWindow( )
        .click(Selector('.ng-binding').nth(2))
        .expect(getLocation()).contains("changed");
        //.expect(Selector('.ng-binding').nth(1).textContent).eql("Test Additional Maila Engineer")
        //.expect(Selector('.ng-binding').nth(2).textContent).contains("changed text");
});

test('ST_009Przesuwanie_mapki', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('#list-item-2MHGBIWY').find('span').withText('changed text'))
        .click(Selector('.btn-map').find('img'))
        .expect(Selector('.marker.backend.animate').getStyleProperty('left')).contains("-16.0709px")
        .expect(Selector('.marker.backend.animate').getStyleProperty('top')).contains("-64.1596px")
        .click(Selector('.glyphicon.glyphicon-menu-right'))
        .expect(Selector('.marker.java.animate').getStyleProperty('left')).contains("-16.0709px")
        .expect(Selector('.marker.java.animate').getStyleProperty('top')).contains("-64.1596px")
        .click(Selector('.marker.react').nth(3))
        .expect(Selector('.marker.react.animate').getStyleProperty('left')).contains("-16.0821px")
        .expect(Selector('.marker.react.animate').getStyleProperty('top')).contains("-64.2929px");
});

test('ST_010Centrowanie_mapki', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('.btn-map').find('img'))
        .click(Selector('.menu-button'))
        .click(Selector('.common-button.ng-scope').nth(13).find('span').withText('Warszawa'))
        .click(Selector('button').withText('Close'))
        .takeScreenshot('undefined')
        .click(Selector('.menu-button'))
        .click(Selector('.common-button.ng-scope').nth(14).find('span').withText('Krakow'))
        .click(Selector('button').withText('Close'))
        .takeScreenshot('undefined')
        .click(Selector('.menu-button'))
        .click(Selector('.common-button.ng-scope').nth(15).find('span').withText('Wroclaw'))
        .click(Selector('button').withText('Close'))
        .takeScreenshot('undefined');
});

test('ST_011Tagowanie', async t => {
    await t
        .maximizeWindow( )
        .typeText(Selector('.ng-pristine.ng-untouched.ng-valid.ng-empty'), 'warsaw')
        .click(Selector('#searchButton'))
        .expect(Selector('#header').find('.ng-binding').textContent).eql("city=warszawa")
        .click(Selector('.menu-button'))
        .click(Selector('.common-button.ng-scope').nth(14).find('span').withText('Krakow'))
        .click(Selector('button').withText('Close'))
        .expect(Selector('#header').find('.ng-binding').textContent).contains("krakow");
});

test('ST_016Nagłówki', async t => {
const getLocation = ClientFunction(() => document.location.href);

await t
        .maximizeWindow( )
        .navigateTo('/jobs/warszawa')
        .expect(getLocation()).contains('/jobs/warszawa')
        .expect(Selector('h1').withText('IT JOB OFFERS IN WARSZAWA').textContent).eql("IT job offers in Warszawa:")
        .expect(Selector('a').withText('View more backend jobs').textContent).eql("+ View more backend jobs")
        .navigateTo('/jobs/android')
        .expect(getLocation()).contains('/jobs/android')
        .expect(Selector('h1').withText('ANDROID JOBS').textContent).eql("Android jobs:")
        .navigateTo('/jobs/warszawa/android')
        .expect(getLocation()).contains('/jobs/warszawa/android')
        .expect(Selector('h1').withText('ANDROID JOBS IN WARSZAWA').textContent).eql("Android jobs in Warszawa:")
        .navigateTo('/jobs/backend')
        .expect(getLocation()).contains('/jobs/backend')
        .expect(Selector('h1').withText('IT JOB OFFERS').textContent).eql("IT job offers:")
        .navigateTo('/jobs/warszawa/backend')
        .expect(getLocation()).contains('/jobs/warszawa/backend')
        .expect(Selector('h1').withText('IT JOB OFFERS IN WARSZAWA').textContent).eql("IT job offers in Warszawa:")
});



test('ST_017Hamburger_Menu', async t => {
    await t
        .maximizeWindow( )
        .expect(Selector('.glyphicon.glyphicon-menu-hamburger').exists).eql(true)
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .expect(Selector('#slide-menu').exists).eql(true)
        .expect(Selector('a').withText('FOR EMPLOYERS').textContent).eql("FOR EMPLOYERS")
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .expect(Selector('a').withText('STORE').textContent).eql("STORE")
        .expect(Selector('a').withText('BLOG').textContent).eql("BLOG")
        .expect(Selector('a').withText('ABOUT').textContent).eql("ABOUT")
        .expect(Selector('a').withText('SURVEYS').textContent).eql("SURVEYS")
        .expect(Selector('a').withText('SURVEYS').textContent).eql("SURVEYS")
        .expect(Selector('a').withText('FEEDBACK').textContent).eql("FEEDBACK")
        .expect(Selector('a').withText('POST A JOB').textContent).eql("POST A JOB")
        .expect(Selector('#slide-menu').find('a').withText('GDPR').textContent).eql("GDPR")
        .expect(Selector('a').withText('IT SALARIES REPORT').textContent).eql("IT SALARIES REPORT")
        .expect(Selector('a').withText('CLIENTS').textContent).eql("CLIENTS")
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('STORE'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        //.click(Selector('a').withText('BLOG'))
        //.navigateTo('http://release.nfj.io')
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('ABOUT'))
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('SURVEYS'))
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('FEEDBACK'))
        .expect(Selector('h1').withText('Send feedback to No Fluff Jobs').textContent).eql("Send feedback to No Fluff Jobs!");
});

test('ST_018Footer', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    await t
        .maximizeWindow( )
        .click(Selector('button').withText('OK'))
        .expect(Selector('a').withText('About us').exists).eql(true)
        .expect(Selector('a').withText('For employers').nth(2).exists).eql(true)
        .expect(Selector('a').withText('Surveys').exists).eql(true)
        .expect(Selector('a').withText('Feedback').exists).eql(true)
        .expect(Selector('a').withText('Careers').exists).eql(true)
        .expect(Selector('a').withText('Subscribe').exists).eql(true)
        .expect(Selector('a').withText('GDPR').nth(1).exists).eql(true)
        .expect(Selector('a').withText('Blog').nth(1).exists).eql(true)
        .expect(Selector('a').withText('Store').nth(1).exists).eql(true)
        .expect(Selector('a').withText('IT Salaries report').exists).eql(true)
        .expect(Selector('a').withText('Clients').exists).eql(true)
        .expect(Selector('a').withText('Terms').exists).eql(true)
        .expect(Selector('.col-xs-6.col-sm-4.col-md-3').nth(1).find('a').withText('Policy').exists).eql(true)
        .expect(Selector('.socialize').find('.btn.btn-circle.btn-info.btn-sm[alt="Send survey!"]').exists).eql(true)
        .expect(Selector('[title="Follow us on Twitter!"]').find('.twitter-image').exists).eql(true)
        .expect(Selector('[alt="Like us on Facebook!"]').exists).eql(true)
        .expect(Selector('.socialize').find('[alt="RSS"]').exists).eql(true)
        .click(Selector('a').withText('About us'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.col-xs-6.col-sm-4.col-md-3').find('a').withText('For employers'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Surveys'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Feedback'))        
        .expect(Selector('.panel-heading').find('h1').textContent).eql("Send feedback to No Fluff Jobs!")    
        .click(Selector('#feedback_modal_cancel'))
        .click(Selector('a').withText('Careers'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Subscribe'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.col-xs-6.col-sm-4.col-md-3').find('a').withText('GDPR'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Policy').nth(0))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.col-xs-6.col-sm-4.col-md-3').nth(1).find('a').withText('Store'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('IT Salaries report'))
        .navigateTo('http://release.nfj.io')             
        //.click(Selector('a').withText('Clients'))  
        await t      
        .click(Selector('li').withText('Terms').find('a'))
        .navigateTo('http://release.nfj.io')  
        await t      
        //.click(Selector('.col-xs-6.col-sm-4.col-md-3').nth(1).find('a').withText('Blog'));
});

test('SM_001Wyświetlanie_Kategorii', async t => {
await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
    //1.Sprawdzenie kategorii Backend jobs
        .expect(Selector('a').withText('BACKEND JOBS').textContent).eql("Backend jobs");
    //2.Sprawdzenie kategorii Full Stack job 
await t
        .expect(Selector('a').withText('FULL STACK JOBS').textContent).eql("Full Stack jobs")
    //3.Sprawdzenie kategorii Mobile Embedded jobs 
        .expect(Selector('a').withText('MOBILE & EMBEDDED JOBS').textContent).eql("Mobile & Embedded jobs")
    //4.Sprawdzenie kategorii Frontend jobs  
        .expect(Selector('a').withText('FRONTEND JOBS').textContent).eql("Frontend jobs")
    //5.Sprawdzenie kategorii Testing jobs 
        .expect(Selector('a').withText('TESTING JOBS').textContent).eql("Testing jobs")
    //6.Sprawdzenie kategorii DevOps jobs 
        .expect(Selector('a').withText('DEVOPS JOBS').textContent).eql("DevOps jobs")
    //7.Sprawdzenie kategorii Business Intelligence jobs
        .expect(Selector('a').withText('BUSINESS INTELLIGENCE JOBS').textContent).eql("Business Intelligence jobs")
    //8.Sprawdzenie kategorii HR jobs
        .expect(Selector('a').withText('HR JOBS').textContent).eql("HR jobs")
    //9.Sprawdzenie kategorii IT Trainee jobs
        .expect(Selector('a').withText('IT TRAINEE JOBS').textContent).eql("IT Trainee jobs")
    //10.Sprawdzenie kategorii UX Designer jobs 
        .expect(Selector('a').withText('UX DESIGNER JOBS').textContent).eql("UX Designer jobs")
    //11.Sprawdzenie kategorii Support jobs 
        .expect(Selector('a').withText('SUPPORT JOBS').textContent).eql("Support jobs")
    //12.Sprawdzenie kategorii Project Manager jobs  
        .expect(Selector('a').withText('PROJECT MANAGER JOBS').textContent).eql("Project Manager jobs")
    //13.Sprawdzenie kategorii Business Analyst jobs
        .expect(Selector('a').withText('BUSINESS ANALYST JOBS').textContent).eql("Business Analyst jobs")
    //14.Sprawdzenie kategorii Other jobs 
        .expect(Selector('a').withText('OTHER JOBS').textContent).eql("Other jobs")
});

test('SM_002Wyświetlanie_listy_firm', async t => {
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('span').withText('Companies'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('0'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('#content-list').find('span').withText('A'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('B'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(3).find('span').withText('C'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('D'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('span').withText('E'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('#content-list').find('span').withText('F'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount ).gt(0)
        .click(Selector('#content-list').find('span').withText('G'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount ).gt(0)
        .click(Selector('#content-list').find('span').withText('H'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount ).gte(0)
        .click(Selector('#content-list').find('span').withText('I'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('J'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('K'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(12).find('span').withText('L'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('M'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('N'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(15).find('span').withText('O'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(16).find('span').withText('P'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('span').withText('Q'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('R'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('S'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(20).find('span').withText('T'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('U'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('span').withText('V'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('.companies-filter-char-wrapper').nth(23).find('span').withText('W'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('#content-list').find('span').withText('X'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('span').withText('Y'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('span').withText('Z'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gte(0)
        .click(Selector('span').withText('Z'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('[class^="toggle-switch ng-pristine ng-untouched ng-valid ng"]'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
        .click(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').find('div').find('div').find('input'))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').nth(7).find('div').find('div').find('company-list-component').find('div').find('section').find('div').nth(41).find('div').childElementCount).gt(0)
});

test('SM_003Wyświetlanie_ogłoszenia', async t => {
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
        .click(Selector('.job-offer-title').nth(0))
        //.expect(Selector('.ng-binding').nth(1).visible).eql(true)
        .expect(Selector('.link-disabled').find('a').visible).eql(true)
        .expect(Selector('.article-header-navigation').find('link-next').find('a').visible).eql(true)
        .expect(Selector('.panel.border-top.border-top1.ng-isolate-scope').find('div').withText('ESSENTIALS').visible).eql(true)
        .expect(Selector('.panel.border-top.border-top2').find('div').withText('SPECS').visible).eql(true)
        .expect(Selector('[class^="panel margin-bottom-10 border-top ng-isolate-scope"]').find('div').withText('PROJECT').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10.ng-isolate-scope').nth(1).find('div').withText('ONE').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10').nth(2).find('div').withText('JOB PROFILE').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10').nth(3).find('div').withText('EQUIPMENT SUPPLIED').visible).eql(true)
        .expect(Selector('.panel.margin-bottom-10').nth(4).find('div').withText('TECHNOLOGIES USED').visible).eql(true)
        .expect(Selector('.panel').nth(7).find('div').withText('OTHER INFO').visible).eql(true)
        .expect(Selector('[class^="panel border-top this-and-that ng-isolate-scope bo"]').find('div').withText('THIS AND THAT').visible).eql(true)
        .expect(Selector('[class^="panel border-top border-top3 margin-bottom-10 ng-i"]').find('div').withText('REQUIREMENTS').visible).eql(true)
        .expect(Selector('.panel-heading').visible).eql(true)
        .expect(Selector('h2').withText('SEE ALSO SIMILAR ADS').visible).eql(true)
});

test('SM_004Wyświetlanie_profilu_firmy', async t => {
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
        .click(Selector('span').withText('Companies'))
        .click(Selector('.company-item.pin-animate').nth(0))
        .expect(Selector('body').find('div').nth(2).find('section').find('div').find('div').find('div').find('company-component').find('div').find('article-header').find('div').nth(4).find('div').find('div').find('div').find('div').find('div').find('div').find('h1').visible).eql(true)
        .expect(Selector('h1').withText('About company').visible).eql(true)
        .expect(Selector('h1').withText('Technologies').visible).eql(true)
        .expect(Selector('h1').withText('Benefits').visible).eql(true)
        .expect(Selector('h1').withText('Job Offers').visible).eql(true)
        //.expect(Selector('#company-location').visible).eql(true)
});

test('SM_005Poprawność_Linków_Ogłoszeń', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    var title = Selector('.ng-binding').nth(1).textContent
    const company = Selector('.ng-binding').nth(2).textContent
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
        .click(Selector('.ng-binding').nth(137))
        .expect(getLocation()).contains("changed");
        //.expect(Selector('.ng-binding').nth(1).textContent).eql("Test Additional Maila Engineer")
        //.expect(Selector('.ng-binding').nth(2).textContent).contains("changed text");
});

test('SM_006Tagowanie', async t => {
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .typeText(Selector('.search.no-margin-bottom.ng-pristine.ng-valid').find('.ng-pristine.ng-untouched.ng-valid.ng-empty'), 'warsaw')
        .click(Selector('#searchButton'))
        .expect(Selector('.span_item.tags.ng-isolate-scope').find('span').withText('city').textContent).eql("city=warszawa")
        .click(Selector('button').withText('Advanced search'))
        .click(Selector('div').withText('Location').nth(10).find('.glyphicon.glyphicon-menu-down'))
        .click(Selector('.common-button.ng-scope').nth(61).find('span').withText('Krakow'))
        .expect(Selector('.span_item.tags.ng-isolate-scope').find('span').withText('city').textContent).contains("krakow");
});

test('SM_010Nagłówki', async t => {
const getLocation = ClientFunction(() => document.location.href);

await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
        .navigateTo('/jobs/warszawa')
        .expect(getLocation()).contains('/jobs/warszawa')
        .expect(Selector('h1').withText('IT JOB OFFERS IN WARSZAWA').textContent).eql("IT job offers in Warszawa:")
        .expect(Selector('a').withText('View more backend jobs').textContent).eql("+ View more backend jobs")
        .navigateTo('/jobs/android')
        .expect(getLocation()).contains('/jobs/android')
        .expect(Selector('h1').withText('ANDROID JOBS').textContent).eql("Android jobs:")
        .navigateTo('/jobs/warszawa/android')
        .expect(getLocation()).contains('/jobs/warszawa/android')
        .expect(Selector('h1').withText('ANDROID JOBS IN WARSZAWA').textContent).eql("Android jobs in Warszawa:")
        .navigateTo('/jobs/backend')
        .expect(getLocation()).contains('/jobs/backend')
        .expect(Selector('h1').withText('IT JOB OFFERS').textContent).eql("IT job offers:")
        .navigateTo('/jobs/warszawa/backend')
        .expect(getLocation()).contains('/jobs/warszawa/backend')
        .expect(Selector('h1').withText('IT JOB OFFERS IN WARSZAWA').textContent).eql("IT job offers in Warszawa:")
});



test('SM_011Hamburger_Menu', async t => {
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
        .expect(Selector('.glyphicon.glyphicon-menu-hamburger').exists).eql(true)
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .expect(Selector('#slide-menu').exists).eql(true)
        .expect(Selector('a').withText('FOR EMPLOYERS').textContent).eql("FOR EMPLOYERS")
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .expect(Selector('a').withText('STORE').textContent).eql("STORE")
        .expect(Selector('a').withText('BLOG').textContent).eql("BLOG")
        .expect(Selector('a').withText('ABOUT').textContent).eql("ABOUT")
        .expect(Selector('a').withText('SURVEYS').textContent).eql("SURVEYS")
        .expect(Selector('a').withText('SURVEYS').textContent).eql("SURVEYS")
        .expect(Selector('a').withText('FEEDBACK').textContent).eql("FEEDBACK")
        .expect(Selector('a').withText('POST A JOB').textContent).eql("POST A JOB")
        .expect(Selector('#slide-menu').find('a').withText('GDPR').textContent).eql("GDPR")
        .expect(Selector('a').withText('IT SALARIES REPORT').textContent).eql("IT SALARIES REPORT")
        .expect(Selector('a').withText('CLIENTS').textContent).eql("CLIENTS")
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('STORE'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        //.click(Selector('a').withText('BLOG'))
        //.navigateTo('http://release.nfj.io')
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('ABOUT'))
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('SURVEYS'))
        .click(Selector('.glyphicon.glyphicon-menu-hamburger'))
        .click(Selector('a').withText('FEEDBACK'))
        .expect(Selector('h1').withText('Send feedback to No Fluff Jobs').textContent).eql("Send feedback to No Fluff Jobs!");
});

test('SM_012Footer', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    await t
        .resizeWindowToFitDevice('iphone6' , {portraitOrientation: true} )
        .click(Selector('.btn.btn-cookie-blue.cookie-hide'))
        .expect(Selector('a').withText('About us').exists).eql(true)
        .expect(Selector('a').withText('For employers').exists).eql(true)
        .expect(Selector('a').withText('Surveys').exists).eql(true)
        .expect(Selector('a').withText('Feedback').exists).eql(true)
        .expect(Selector('a').withText('Careers').exists).eql(true)
        .expect(Selector('a').withText('Subscribe').exists).eql(true)
        .expect(Selector('a').withText('GDPR').exists).eql(true)
        .expect(Selector('a').withText('Blog').exists).eql(true)
        .expect(Selector('a').withText('Store').exists).eql(true)
        .expect(Selector('a').withText('IT Salaries report').exists).eql(true)
        .expect(Selector('a').withText('Clients').exists).eql(true)
        .expect(Selector('a').withText('Terms').exists).eql(true)
        .expect(Selector('.col-xs-6.col-sm-4.col-md-3').nth(1).find('a').withText('Policy').exists).eql(true)
        .expect(Selector('.socialize').find('.btn.btn-circle.btn-info.btn-sm[alt="Send survey!"]').exists).eql(true)
        .expect(Selector('[title="Follow us on Twitter!"]').find('.twitter-image').exists).eql(true)
        .expect(Selector('[alt="Like us on Facebook!"]').exists).eql(true)
        .expect(Selector('.socialize').find('[alt="RSS"]').exists).eql(true)
        .click(Selector('a').withText('About us'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.col-xs-6.col-sm-4.col-md-3').find('a').withText('For employers'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Surveys'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Feedback'))        
        .expect(Selector('.panel-heading').find('h1').textContent).eql("Send feedback to No Fluff Jobs!")    
        .click(Selector('#feedback_modal_cancel'))
        .click(Selector('a').withText('Careers'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Subscribe'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.col-xs-6.col-sm-4.col-md-3').find('a').withText('GDPR'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('Policy').nth(0))
        .navigateTo('http://release.nfj.io')
        .click(Selector('.col-xs-6.col-sm-4.col-md-3').nth(1).find('a').withText('Store'))
        .navigateTo('http://release.nfj.io')
        .click(Selector('a').withText('IT Salaries report'))
        .navigateTo('http://release.nfj.io')             
        //.click(Selector('a').withText('Clients'))  
        await t      
        .click(Selector('li').withText('Terms').find('a'))
        .navigateTo('http://release.nfj.io')  
        await t      
        //.click(Selector('.col-xs-6.col-sm-4.col-md-3').nth(1).find('a').withText('Blog'));
});

test('SP_001Sortowanie wg. wybranych kategorii.', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('.glyphicon.glyphicon-chevron-down'))
        .click(Selector('p').withText('Log in to Employer Panel'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .pressKey('tab')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .click(Selector('th').withText('ID'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('ID'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('ID'))
        .click(Selector('th').withText('NAME'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('NAME'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('NAME'))
        .click(Selector('th').withText('COMPANY'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('COMPANY'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('COMPANY'))
        .click(Selector('span').withText('NFJ'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('NFJ'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('NFJ'))
        .click(Selector('span').withText('TYPE'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('TYPE'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('TYPE'))
        .click(Selector('span').withText('FROM'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('FROM'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('FROM'))
        .click(Selector('span').withText('TO'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('TO'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('TO'))
        .click(Selector('th').withText('CITIES'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('CITIES'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('CITIES'))
        .click(Selector('th').withText('CATEGORY'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('CATEGORY'))
        .takeScreenshot('undefined')
        .click(Selector('th').withText('CATEGORY'))
        .click(Selector('[data-st-sort="applications.count.total"].text-center.nfj-office-table__ta.ng-scope').find('span').withText('TA'))
        .takeScreenshot('undefined')
        .click(Selector('[data-st-sort="applications.count.total"].text-center.nfj-office-table__ta.ng-scope').find('span').withText('TA'))
        .takeScreenshot('undefined')
        .click(Selector('[data-st-sort="applications.count.total"].text-center.nfj-office-table__ta.ng-scope').find('span').withText('TA'))
        .click(Selector('span').withText('EA'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('EA'))
        .takeScreenshot('undefined')
        .click(Selector('span').withText('EA'))
        .click(Selector('[data-st-sort="lastEdition.stats.formRedirects"].text-center.nfj-office-table__fr').find('span').withText('FR'))
        .click(Selector('[data-st-sort="lastEdition.stats.formRedirects"].text-center.nfj-office-table__fr').find('span').withText('FR'))
        .click(Selector('[data-st-sort="lastEdition.stats.formRedirects"].text-center.nfj-office-table__fr.st-sort-descent').find('span').withText('FR'))
        .click(Selector('span').withText('TR'))
        .click(Selector('span').withText('TR'))
        .click(Selector('span').withText('TR'))
        .click(Selector('span').withText('EDS'))
        .click(Selector('span').withText('EDS'))
        .click(Selector('span').withText('EDS'))
        .click(Selector('span').withText('POSTED'))
        .click(Selector('span').withText('POSTED'))
        .click(Selector('span').withText('POSTED'))
        .click(Selector('th').withText('RENEWAL'))
        .click(Selector('th').withText('RENEWAL'))
        .click(Selector('th').withText('RENEWAL'))
        .click(Selector('span').withText('EXPIRES'))
        .click(Selector('span').withText('EXPIRES'))
        .click(Selector('span').withText('EXPIRES'))
        .click(Selector('span').withText('STA'))
        .click(Selector('span').withText('STA'))
        .click(Selector('span').withText('STA'))
        .click(Selector('th').withText('ACTIONS'));
});

test('SP_002Działanie_przycisków:All,show stats,itp.', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('button').withText('User panel'))
        .click(Selector('p').withText('Log in to Employer Panel'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .pressKey('tab')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .expect(Selector('.nfj-postings.block').visible).eql(true)
        .click(Selector('label').withText('All'))
        .expect(Selector('.nfj-postings.block').visible).eql(true)
        .click(Selector('label').withText('Editions'))
        .expect(Selector('.nfj-postings.block').visible).eql(true)
        .click(Selector('label').withText('Published'))
        .click(Selector('a').withText('Show stats'))
        .expect(Selector('span').withText('PV').visible).eql(true)
        .expect(Selector('span').withText('UV').visible).eql(true)
        .expect(Selector('span').withText('FB').visible).eql(true)
        .expect(Selector('span').withText('TW').visible).eql(true)
        .expect(Selector('th').withText('RENEWED').visible).eql(true)
        .expect(Selector('span').withText('PRO').visible).eql(true)
        .expect(Selector('button').withText('Promote all').visible).eql(true)
        .expect(Selector('button').withText('Unpromote all').visible).eql(true)
        .click(Selector('a').withText('Hide stats'))
        .expect(Selector('span').withText('PV').exists).eql(false)
        .expect(Selector('span').withText('UV').exists).eql(false)
        .expect(Selector('span').withText('FB').exists).eql(false)
        .expect(Selector('span').withText('TW').exists).eql(false)
        .expect(Selector('th').withText('RENEWED').exists).eql(false)
        .expect(Selector('span').withText('PRO').exists).eql(false)
        .expect(Selector('button').withText('Promote all').exists).eql(false)
        .expect(Selector('button').withText('Unpromote all').exists).eql(false);
});

test('SP_003Dodanie_nowego_usera', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    await t
        .maximizeWindow( )
        .click(Selector('button').withText('User panel'))
        .click(Selector('p').withText('Log in to Employer Panel'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .pressKey('tab')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .click(Selector('a').withText('Users'))
        .click(Selector('button').withText('New User'))
        .typeText(Selector('[name="editUser_form"][class^="form-horizontal ng-scope ng-pristine ng-invalid ng"]').find('[name="email_input"]'), 'test@test')
        .typeText(Selector('[name="editUser_form"][class^="form-horizontal ng-scope ng-invalid ng-invalid-req"]').find('[name="name_input"]'), 'testing')
        .click(Selector('label').withText('CUSTOMER').nth(0))
        .click(Selector('[data-style="expand-left"].primary.ng-scope.ladda-button').find('span').withText('Create'))
        .navigateTo('/panel/#/users/?search=%7B""%7D')
        .expect(Selector('td').withText('test').nth(1).textContent).eql("test@test")
        .expect(Selector('td').withText('testing').textContent).eql("testing");
});

test('SP_004Dodawanie_nowego_Vouchera', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('User panel'))
        .click(Selector('p').withText('Manage job offers and your potential candidates'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .click(Selector('a').withText('Vouchers'))
        .click(Selector('button').withText('New Voucher'))
        .typeText(Selector('[name="vm.editVoucher_form"][data-ng-submit^="vm.editVoucher_form.$valid && vm.createOrUpdate(vm"][class^="form-horizontal nfj-voucher-form ng-scope ng-prist"]').find('[name="owner_input"]'), '@nofluffjobs.com')
        .typeText(Selector('[name="vm.editVoucher_form"][data-ng-submit^="vm.editVoucher_form.$valid && vm.createOrUpdate(vm"][class^="form-horizontal nfj-voucher-form ng-scope ng-inval"]').find('[name="contactPerson_input"]'), '@nofluffjobs.com')
        .click(Selector('span').withText('Generate code').nth(1))
        .typeText(Selector('[name="vm.editVoucher_form"][data-ng-submit^="vm.editVoucher_form.$valid && vm.createOrUpdate(vm"][class^="form-horizontal nfj-voucher-form ng-scope ng-inval"]').find('[name="postingCount_input"]'), '1')
        .click(Selector('[name="multiCityCount_input"][data-ng-model="vm.voucher.multiCityCount"][class^="form-control ng-isolate-scope ng-pristine ng-valid"]'))
        .typeText(Selector('[name="multiCityCount_input"][data-ng-model="vm.voucher.multiCityCount"][class^="form-control ng-isolate-scope ng-pristine ng-valid"]'), '1')
        .click(Selector('[name="midtermRenewalCount_input"][data-ng-model="vm.voucher.midtermRenewalCount"][class^="form-control ng-isolate-scope ng-pristine ng-valid"]'))
        .typeText(Selector('[name="midtermRenewalCount_input"][data-ng-model="vm.voucher.midtermRenewalCount"][class^="form-control ng-isolate-scope ng-pristine ng-valid"]'), '1')
        .click(Selector('span').withText('Create'))
        .expect(getLocation()).contains('/panel/#/vouchers/');
});

test('SP_005Funkcjonowanie_przycisków_actions', async t => {
    const getLocation = ClientFunction(() => document.location.href);
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('User panel'))
        .click(Selector('p').withText('Log in to Employer Panel'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .pressKey('tab')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .click(Selector('[data-ng-href^="http://dev.nfj.io/job/test-additional-maila-engine"].btn.btn-default.btn-xs.ng-isolate-scope').find('.glyphicon.nfj-iconify.glyphicon-eye-open'))
        .expect(getLocation()).contains("/job/")
        .navigateTo('http://release.nfj.io/panel/')
        await t
        .click(Selector('[data-ng-href^="http://dev.nfj.io/posting_edit.html?id=2MHGBIWY&to"].btn.btn-default.btn-xs.ng-isolate-scope').find('.glyphicon.nfj-iconify.glyphicon-pencil'))
        .expect(getLocation()).contains("/posting_edit.")
        .navigateTo('http://release.nfj.io/panel/')
        .click(Selector('.glyphicon.nfj-iconify.glyphicon-search').nth(0))
        .expect(getLocation()).contains("#/similar")
        .click(Selector('a').withText('Office'))
        .click(Selector('.btn.btn-default.btn-xs.ng-isolate-scope').nth(3))
        .expect(getLocation()).contains("/status");
});

test('SP_006Porównywanie_postingów', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('User panel'))
        .click(Selector('p').withText('Log in to Employer Panel'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .pressKey('tab')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .click(Selector('a').withText('Similar'))
        .click(Selector('.form-inline.ng-pristine.ng-valid').find('[name="id"]'))
        .typeText(Selector('.form-inline.ng-pristine.ng-valid').find('[name="id"]'), 'NTZVRENE')
        .pressKey('enter')
        .expect(Selector('body').find('div').find('div').nth(6).find('div').find('div').find('div').find('div').nth(3).find('div').find('div').find('div').find('table').find('tbody').find('tr').count).gte(1);

});

test('SP_007Edytowanie_swojego_profilu', async t => {
    await t
        .maximizeWindow( )
        .click(Selector('span').withText('User panel'))
        .click(Selector('p').withText('Log in to Employer Panel'))
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"].ng-pristine.ng-invalid.ng-invalid-required').find('[name="email_input"]'), 'admin@admin')
        .pressKey('tab')
        .typeText(Selector('[name="signIn_form"][data-ng-submit="signIn_form.$valid && vm.signIn()"][class^="ng-invalid ng-invalid-required ng-dirty ng-valid-e"]').find('[name="password_input"]'), 'dev2238staging')
        .click(Selector('span').withText('Sign In'))
        .click(Selector('a').withText('Profile'))
        .typeText(Selector('.col-xs-7').nth(3).nth(3).find('input'), 'El Admin')
        .click(Selector('[data-ladda="vm.isUserNameChangeInProgress"][data-style="expand-left"].primary.ladda-button').find('span').withText('Change'))
        .expect(Selector('.col-xs-7').nth(2).nth(2).nth(2).find('p').textContent).eql("El Admin")
        .typeText(Selector('[name="changeName_form"][data-ng-submit="changeName_form.$valid && vm.changeUserName()"][class^="form-horizontal ng-dirty ng-invalid ng-invalid-req"]').find('[name="newName_input"]'), 'Elvis Admin')
        .click(Selector('[data-ladda="vm.isUserNameChangeInProgress"][data-style="expand-left"].primary.ladda-button').find('span').withText('Change'))
        .expect(Selector('.col-xs-7').nth(2).nth(2).nth(2).find('p').textContent).eql("Elvis Admin");
});