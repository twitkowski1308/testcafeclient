"@fixture SmokeTestsDesktopStaging";
"@page http://dev.nofluffjobs.com";
"@test"["ST_001Wyświetlanie_Kategorii"] = {
    "1.Wait 1000 milliseconds": function() {
        act.wait(1e3);
    },
    "1.Sprawdzenie kategorii Backend jobs": function() {  
        eq($(":containsExcludeChildren(Backend jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Full Stack jobs": function() {  
        ok($(":containsExcludeChildren(Full Stack jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Mobile Embedded jobs": function() {  
        ok($(":containsExcludeChildren(Mobile Embedded jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Frontend jobs": function() {  
        ok($(":containsExcludeChildren(Frontend jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Testing jobs": function() {  
        ok($(":containsExcludeChildren(Testing jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii DevOps jobs": function() {  
        ok($(":containsExcludeChildren(DevOps jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Business Intelligence jobs": function() {  
        ok($(":containsExcludeChildren(Business Intelligence jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii HR jobs": function() {  
        ok($(":containsExcludeChildren(HR jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii IT Trainee jobs": function() {  
        ok($(":containsExcludeChildren(IT Trainee jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii UX Designer jobs": function() {  
        ok($(":containsExcludeChildren(UX Designer jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Support jobs": function() {  
        ok($(":containsExcludeChildren(Support jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Project Manager jobs": function() {  
        ok($(":containsExcludeChildren(Project Manager jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Business Analyst jobs": function() {  
        ok($(":containsExcludeChildren(Business Analyst jobs)").length > 0, true);
    },
    "1.Sprawdzenie kategorii Other jobs": function() {  
        ok($(":containsExcludeChildren(Other jobs)").length > 0, true);
    },
    "2.Włączenie mapy": function() {
        act.click(".slider.round");
    },
    "1.Wait 1000 milliseconds": function() {
        act.wait(1e3);
    },
    "3.Sprawdzenie widoczności mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "4.Wyłączenie mapy": function() {
        act.click(".slider.round");
    },
    "5.Sprawdzenie widoczności mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_002WyświetlanieListyFirm"] = {
    '1.Kliknięcie w Companies': function() {
        act.click(":containsExcludeChildren(Companies)");
    },
    "2.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '3.Kliknięcie w [0-9]"': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(09)");
        };
        act.click(actionTarget);
    },
    "4.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(2) > div:nth(0) ").children().length, 0);
    },
    '5.Kliknięcie w [A]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(A)");
        };
        act.click(actionTarget);
    },
    "6.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '7.Kliknięcie w [B]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(B)");
        };
        act.click(actionTarget);
    },
    "8.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '9.Kliknięcie w [C]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(C)").eq(0);
        };
        act.click(actionTarget);
    },
    "10.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '11.Kliknięcie w [D]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(D)");
        };
        act.click(actionTarget);
    },
    "12.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(2) ").children().length, 0);
    },
    '13.Kliknięcie w [E]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(E)");
        };
        act.click(actionTarget);
    },
    "14.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(2) > div:nth(0) ").children().length, 0);
    },
    '15.Kliknięcie w [F]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(F)");
        };
        act.click(actionTarget);
    },
    "16.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '17.Kliknięcie w [G]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(G)");
        };
        act.click(actionTarget);
    },
    "18.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '19.Kliknięcie w [H]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(H)");
        };
        act.click(actionTarget);
    },
    "20.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(2) ").children().length, 0);
    },
    '21.Kliknięcie w [I]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(I)");
        };
        act.click(actionTarget);
    },
    "22.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '23.Kliknięcie w [J]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(J)");
        };
        act.click(actionTarget);
    },
    "24.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '25.Kliknięcie w [K]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(K)");
        };
        act.click(actionTarget);
    },
    "26.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '27.Kliknięcie w [L]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(L)").eq(0);
        };
        act.click(actionTarget);
    },
    "28.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '29.Kliknięcie w [M]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(M)");
        };
        act.click(actionTarget);
    },
    "30.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '31.Kliknięcie w [N]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(N)");
        };
        act.click(actionTarget);
    },
    "32.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '33.Kliknięcie w [O]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(O)").eq(0);
        };
        act.click(actionTarget);
    },
    "34.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '35.Kliknięcie w [P]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(P)").eq(0);
        };
        act.click(actionTarget);
    },
    "36.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(2) ").children().length, 0);
    },
    '37.Kliknięcie w [Q]': function() {
        act.click(":containsExcludeChildren(Q)");
    },
    "38.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '39.Kliknięcie w [R]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(R)").eq(1);
        };
        act.click(actionTarget);
    },
    "40.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '41.Kliknięcie w [S]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(S)").eq(1);
        };
        act.click(actionTarget);
    },
    "42.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '43.Kliknięcie w [T]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(T)").eq(0);
        };
        act.click(actionTarget);
    },
    "44.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '45.Kliknięcie w [U]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(U)");
        };
        act.click(actionTarget);
    },
    "46.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '47.Kliknięcie w [V]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(V)");
        };
        act.click(actionTarget);
    },
    "48.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '49.Kliknięcie w [W]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(W)").eq(0);
        };
        act.click(actionTarget);
    },
    "50.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '51.Kliknięcie w [X]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(X)");
        };
        act.click(actionTarget);
    },
    "52.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '53.Kliknięcie w [Y]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(Y)");
        };
        act.click(actionTarget);
    },
    "54.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(3) > div:nth(0) ").children().length, 0);
    },
    '55.Kliknięcie w [Z]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(Z)");
        };
        act.click(actionTarget);
    },
    "56.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > company-list-component:nth(0) > div:nth(0) > section:nth(0) > div:nth(2) > div:nth(0)").children().length, 0);
    },
    '57.Odliknięcie [Z]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(Z)");
        };
        act.click(actionTarget);
    },
    "58.Włączenie mapy": function() {
        act.click(".slider.round");
    },
    "59.Sprawdzenie widoczności mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "60.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    "61.Wyłączenie mapy": function() {
        act.click(".slider.round");
    },
    "62.Sprawdzenie widoczności mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_003WyświetlanieOgłoszenia"] = {
    '1.Click span "Senior .Net..."': function() {
        act.click(":containsExcludeChildren(Senior Net Developer)");
    },
    "2.Sprawdzenie wyświetlania kategorii": function() {
        ok($(".col-flex.remove-padding").find(" > h1:nth(0)").length > 0);
        ok($(":containsExcludeChildren(REQUIREMENTS)").length > 0);
        ok($(":containsExcludeChildren(Must have)").length > 0);
        ok($(":containsExcludeChildren(Nicetohave)").length > 0);
        ok($(":containsExcludeChildren(Languages)").length > 0);
        ok($(":containsExcludeChildren(WORK METHODOLOGY)").length > 0);
        ok($(":containsExcludeChildren(JOB PROFILE)").length > 0);
        ok($(":containsExcludeChildren(EQUIPMENT SUPPLIED)").length > 0);
        ok($(":containsExcludeChildren(SPECS)").length > 0);
        ok($(":containsExcludeChildren(ESSENTIALS)").length > 0);
        ok($(":containsExcludeChildren(ONEONONE)").length > 0);
        ok($(":containsExcludeChildren(this and that)").length > 0);
        ok($(":containsExcludeChildren(OTHER INFO)").length > 0);
        ok($("#tools-block").length > 0);
        ok($(":containsExcludeChildren(Apply)").length > 0);
    },
    "3.Włączenie mapy": function() {
        act.click(".slider.round");
    },
    "4.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "5.Wyłączenie mapy": function() {
        act.click(".slider.round");
    },
    "4.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_004WyświetlanieProfiluFirmy"] = {
    '1.Kliknięcie w "Companies"': function() {
        act.click(":containsExcludeChildren(Companies)");
    },
    "2.Kliknięcie w firmę z listy": function() {
        var actionTarget = function() {
            return $(".company-item.pin-animate").eq(0);
        };
        act.click(actionTarget);
    },
    "3.Sprawdzenie wyświetlania kategorii": function() {
        ok($(".row").find(" > h1:nth(0)").eq(0).length > 0);
        ok($(":containsExcludeChildren(About company)").length > 0);
        ok($("#technologies-and-benefits").find(":containsExcludeChildren(Technologies)").length > 0);
        ok($("#technologies-and-benefits").find(":containsExcludeChildren(Benefits)").length > 0);
    },
    "4.Włączenie mapy": function() {
        act.click(".slider.round");
    },
    "1.Wait 1000 milliseconds": function() {
        act.wait(1e3);
    },
    "5.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "6.Wyłączenie mapy": function() {
        act.click(".slider.round");
    },
    "5.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_005PorównywanieOfert"] = {
    "1.Hover over link": function() {
        act.hover("#list-item-NTZVRENE");
    },
    "2.Click span": function() {
        act.click("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(0) > a:nth(0) > div:nth(1) > button:nth(0) > span:nth(0)");
    },
    "3.Hover over link": function() {
        act.hover("#list-item-NFQS9NXQ");
    },
    "4.Click span": function() {
        act.click("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > a:nth(0) > div:nth(1) > button:nth(0) > span:nth(0)");
    },
    "5.Hover over link": function() {
        act.hover("#list-item-RCPNMNWN");
    },
    "6.Click span": function() {
        act.click("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(2) > a:nth(0) > div:nth(1) > button:nth(0) > span:nth(0)");
    },
    "7.Hover over div": function() {
        var actionTarget = function() {
            return $(".panel-body").eq(1);
        };
        act.hover(actionTarget);
    },
    "8.Click span": function() {
        act.click(".glyphicon.glyphicon-shopping-cart");
    },
    "9.Assert": function() {
        ok($("#container").find(".panel-body.mobile-top").eq(3).length > 0);
        ok($("#container").find(".panel-body.mobile-top").eq(4).length > 0);
        ok($("#container").find(".panel-body.mobile-top").eq(5).length > 0);
        ok($("#container").find(".panel-body").eq(7).length > 0);
        ok($("#container").find(".panel-body").eq(3).length > 0);
        ok($("#container").find(".panel-body").eq(11).length > 0);
        ok($("#container").find(".panel-body").eq(15).length > 0);
        ok($("#container").find(".panel-body").eq(19).length > 0);
        ok($("#container").find(".panel-body").eq(23).length > 0);
        ok($("#container").find(".panel-body").eq(27).length > 0);
        ok($("#container").find(".panel-body").eq(31).length > 0);
        ok($("#container").find(".panel-body").eq(35).length > 0);
        ok($("#container").find(".panel-body").eq(39).length > 0);
        ok($("#container").find(".panel-body").eq(43).length > 0);
        ok($("#container").find(".panel-body").eq(47).length > 0);
        ok($("#container").find(".panel-body").eq(51).length > 0);
        ok($("#container").find(".panel-body").eq(55).length > 0);
        ok($("#container").find(".panel-body").eq(59).length > 0);
        ok($("#container").find(".panel-body").eq(62).length > 0);
        ok($(".panel-body").eq(1).length > 0);
        ok($(".panel-body").eq(2).length > 0);
        ok($(".panel-body").eq(3).length > 0);
        ok($(".panel-body").eq(4).length > 0);
        ok($(".panel-body").eq(5).length > 0);
        ok($(".panel-body").eq(6).length > 0);
        ok($(".panel-body").eq(7).length > 0);
        ok($(".panel-body").eq(8).length > 0);
        ok($(".panel-body").eq(9).length > 0);
        ok($(".panel-body").eq(10).length > 0);
    },
    "10.Assert": function() {
        eq($("body > div:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > table:nth(0) > tbody:nth(0) > tr:nth(1)  ").children().length, 4);
    },
    "11.Hover over div": function() {
        var actionTarget = function() {
            return $(".compare-widget.panel").find(" > div:nth(1)");
        };
        act.hover(actionTarget);
    },
    "12.Click span": function() {
        var actionTarget = function() {
            return $(".glyphicon.glyphicon-remove.compare-widget-remove.nfj-red.ng-scope").eq(0);
        };
        act.click(actionTarget);
    },
    "13.Assert": function() {
        eq($("body > div:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > table:nth(0) > tbody:nth(0) > tr:nth(1)  ").children().length, 3);
    }
};


"@test"["ST_007PrzesuwanieMapki"] = {
    '1.Cookie accept "OK, I got it"': function() {
        act.click(":containsExcludeChildren(OK I got it)");
    },
    '2.Kliknięcie w ofertę "Software (Python /..."': function() {
        act.click("body > posting-navigation:nth(0) > section:nth(0) > div:nth(2) > div:nth(1) > div:nth(0) > postings-component:nth(0) > section:nth(1) > div:nth(0) > div:nth(1) > a:nth(0) > h3:nth(0) > span:nth(0)");
    },
    "3.Włączenie mapki": function() {
        act.click(".slider.round");
    },
    "4.Kliknięcie w search": function() {
        act.click(".header-wrapper");
    },
    "5.Sprawdzenine pozycji na mapie": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(4) > div > div").attr("style"), "display: block; position: absolute; left: -15.5993px; top: -63.6606px;");
    },
    "6.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    '7.Przejście do nastepnej oferty': function() {
        act.click(":containsExcludeChildren(Next)");
    },
    "8.Sprawdzenine pozycji na mapie": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(4) > div > div").attr("style"), "display: block; left: -15.5756px; top: -64.1916px; position: absolute;");
    },
    "9.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    '10.Przejście do nastepnej oferty': function() {
        act.click(":containsExcludeChildren(Next)");
    },
    "11.Sprawdzenine pozycji na mapie": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(4) > div > div").attr("style"), "display: block; left: -16.0288px; top: -64.4816px; position: absolute;");
    },
    "12.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    "13.Wyłączenine mapki": function() {
        act.click(".slider.round");
    }
};

"@test"["ST_008CentrowanieMapkiFail"] = {
    "1.Click span": function() {
        act.click(".slider.round");
    },
    "2.Click image": function() {
        var actionTarget = function() {
            return $(".btn.btn-transparent.btn-transparent-light.menu-button").find(" > img:nth(0)");
        };
        act.click(actionTarget);
    },
    '3.Click span "Warszawa"': function() {
        var actionTarget = function() {
            return $("#header").find(":containsExcludeChildren(Warszawa)").eq(0);
        };
        act.click(actionTarget);
    },
    '4.Click submit button "Close"': function() {
        act.click(":containsExcludeChildren(Close)");
    },
    "5.Assert": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(5) > div > div:nth-child(1) > img").attr("src"), "https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4574!3i2697!4i256!2m3!1e0!2sm!3i439144092!3m9!2spl-PL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&key=AIzaSyCA0EXdJTinEazsISXEP4EzlN_ZOKOrmm4&token=122747");
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(5) > div > div:nth-child(2) > img").attr("src"), "https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4573!3i2697!4i256!2m3!1e0!2sm!3i439144092!3m9!2spl-PL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&key=AIzaSyCA0EXdJTinEazsISXEP4EzlN_ZOKOrmm4&token=118077");
    },
    "6.Click image": function() {
        var actionTarget = function() {
            return $(".btn.btn-transparent.btn-transparent-light.menu-button").find(" > img:nth(0)");
        };
        act.click(actionTarget);
    },
    '7.Click span "Lodz"': function() {
        var actionTarget = function() {
            return $(":containsExcludeChildren(Lodz)").eq(0);
        };
        act.click(actionTarget);
    },
    '8.Click span "Wroclaw"': function() {
        var actionTarget = function() {
            return $(":containsExcludeChildren(Wroclaw)").eq(0);
        };
        act.click(actionTarget);
    },
    '9.Click submit button "Close"': function() {
        act.click(":containsExcludeChildren(Close)");
    },
    "10.Assert": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(1) > div:nth-child(5) > div > div:nth-child(1) > img").attr("src"), "https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i70!3i42!4i256!2m3!1e0!2sm!3i439144092!3m9!2spl-PL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&key=AIzaSyCA0EXdJTinEazsISXEP4EzlN_ZOKOrmm4&token=28613");
    },
    "11.Click span": function() {
        act.click(".slider.round");
    },
    "12.Click span": function() {
        act.click(".slider.round");
    }
};

"@test"["ST_009Tagowanie"] = {
    "1.Type in input": function() {
        act.type(".ng-pristine.ng-untouched.ng-valid.ng-empty", "warsaw");
    },
    "2.Press key ENTER": function() {
        act.press("enter");
    },
    "3.Assert": function() {
        eq($(".span_item.tags.ng-isolate-scope").find(" > span:nth(0)").text(), "city=warszawa");
    },
    "4.Type in input": function() {
        act.type(".ng-untouched.ng-valid.ng-dirty.ng-empty", "lodz");
    },
    "5.Press key ENTER": function() {
        act.press("enter");
    },
    "6.Assert": function() {
        eq($(".span_item.tags.ng-isolate-scope").find(" > span:nth(0)").text(), "city=warszawa,lodz");
    },
    "7.Click submit button": function() {
        act.click(".btn.btn-transparent.btn-transparent-light.menu-button");
    },
    '8.Click span "python"': function() {
        var actionTarget = function() {
            return $(":containsExcludeChildren(python)").eq(0);
        };
        act.click(actionTarget);
    },
    '9.Click span "android"': function() {
        var actionTarget = function() {
            return $(":containsExcludeChildren(android)").eq(0);
        };
        act.click(actionTarget);
    },
    '10.Click submit button "Close"': function() {
        act.click(":containsExcludeChildren(Close)");
    },
    "11.Assert": function() {
        eq($("body > posting-navigation:nth(0) > header:nth(0) > div:nth(1) > search-header:nth(0) > search-tags-component:nth(0) > div:nth(0) > ul:nth(0) > li:nth(0) > span:nth(0) > span:nth(0)").text(), "python");
        eq($("#search-tags-component").find(".ng-binding").eq(1).text(), "android");
    }
};