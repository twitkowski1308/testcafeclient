"@fixture SmokeTestsDesktopRelease";
"@page http://release.nofluffjobs.com/";
"@test"["ST_001Wyświetlanie_Kategorii"] = {
    "1.Sprawdzenie kategorii Backend jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(0).text(), "Backend jobs");
    },
    "2.Sprawdzenie kategorii Full Stack jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(1).text(), "Full Stack jobs");
    },
    "3.Sprawdzenie kategorii Mobile Embedded jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(2).text(), "Mobile & Embedded jobs");
    },
    "4.Sprawdzenie kategorii Frontend jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(3).text(), "Frontend jobs");
    },
    "5.Sprawdzenie kategorii Testing jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(4).text(), "Testing jobs");
    },
    "6.Sprawdzenie kategorii DevOps jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(5).text(), "DevOps jobs");
    },
    "7.Sprawdzenie kategorii Business Intelligence jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(6).text(), "Business Intelligence jobs");
    },
    "8.Sprawdzenie kategorii HR jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(7).text(), "HR jobs");
    },
    "9.Sprawdzenie kategorii IT Trainee jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(8).text(), "IT Trainee jobs");
    },
    "10.Sprawdzenie kategorii UX Designer jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(9).text(), "UX Designer jobs");
    },
    "11.Sprawdzenie kategorii Support jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(10).text(), "Support jobs");
    },
    "12.Sprawdzenie kategorii Project Manager jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(11).text(), "Project Manager jobs");
    },
    "13.Sprawdzenie kategorii Business Analyst jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(12).text(), "Business Analyst jobs");
    },
    "14.Sprawdzenie kategorii Other jobs": function() {  
        eq($(".category-e2e.ng-binding").eq(13).text(), "Other jobs");
    },
    "15.Włączenie mapki": function() {
        var actionTarget = function() {
            return $(".btn-map").find(" > img:nth(0)");
        };
        act.click(actionTarget);
    },
    "16.Oczekiwanie na załadowanie": function() {
        act.wait(2e3);
    },
    "17.Sprawdzenie widoczności mapki": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "18.Wyłączenie mapki": function() {
        act.click(".map-close.btn");
    },
    "19.Sprawdzenie widoczności mapki": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_002WyświetlanieListyFirm"] = {
    "2.Oczekiwanie na załadowanie": function() {
        act.wait(2e3);
    },
    '1.Kliknięcie w Companies': function() {
        act.click("body > div:nth(1) > header:nth(0) > div:nth(0) > div:nth(0) > div:nth(0) > a:nth(1) > span:nth(0)");
    },
    "2.Oczekiwanie na załadowanie": function() {
        act.wait(2e3);
    },
    "2.Sprawdzenie wyświetlania listy firm": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '3.Kliknięcie w [0-9]"': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(09)");
        };
        act.click(actionTarget);
    },
    "4.Sprawdzenie wyświetlania listy firm na 0-9": function() {
        eq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '5.Kliknięcie w [A]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(A)");
        };
        act.click(actionTarget);
    },
    "6.Sprawdzenie wyświetlania listy firm na A": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '7.Kliknięcie w [B]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(B)");
        };
        act.click(actionTarget);
    },
    "8.Sprawdzenie wyświetlania listy firm na B": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '9.Kliknięcie w [C]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(C)").eq(0);
        };
        act.click(actionTarget);
    },
    "10.Sprawdzenie wyświetlania listy firm na C": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '11.Kliknięcie w [D]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(D)");
        };
        act.click(actionTarget);
    },
    "12.Sprawdzenie wyświetlania listy firm na D": function() {
        eq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '13.Kliknięcie w [E]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(E)");
        };
        act.click(actionTarget);
    },
    "14.Sprawdzenie wyświetlania listy firm na E": function() {
        eq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '15.Kliknięcie w [F]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(F)");
        };
        act.click(actionTarget);
    },
    "16.Sprawdzenie wyświetlania listy firm na F": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '17.Kliknięcie w [G]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(G)");
        };
        act.click(actionTarget);
    },
    "18.Sprawdzenie wyświetlania listy firm na G": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '19.Kliknięcie w [H]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(H)");
        };
        act.click(actionTarget);
    },
    "20.Sprawdzenie wyświetlania listy firm na H": function() {
        eq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '21.Kliknięcie w [I]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(I)");
        };
        act.click(actionTarget);
    },
    "22.Sprawdzenie wyświetlania listy firm na I": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '23.Kliknięcie w [J]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(J)");
        };
        act.click(actionTarget);
    },
    "24.Sprawdzenie wyświetlania listy firm na J": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '25.Kliknięcie w [K]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(K)");
        };
        act.click(actionTarget);
    },
    "26.Sprawdzenie wyświetlania listy firm na K": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '27.Kliknięcie w [L]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(L)").eq(0);
        };
        act.click(actionTarget);
    },
    "28.Sprawdzenie wyświetlania listy firm na L": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '29.Kliknięcie w [M]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(M)");
        };
        act.click(actionTarget);
    },
    "30.Sprawdzenie wyświetlania listy firm na M": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '31.Kliknięcie w [N]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(N)");
        };
        act.click(actionTarget);
    },
    "32.Sprawdzenie wyświetlania listy firm na N": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '33.Kliknięcie w [O]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(O)").eq(0);
        };
        act.click(actionTarget);
    },
    "34.Sprawdzenie wyświetlania listy firm na O": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '35.Kliknięcie w [P]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(P)").eq(0);
        };
        act.click(actionTarget);
    },
    "36.Sprawdzenie wyświetlania listy firm na P": function() {
        eq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '37.Kliknięcie w [Q]': function() {
        act.click(":containsExcludeChildren(Q)");
    },
    "38.Sprawdzenie wyświetlania listy firm na Q": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '39.Kliknięcie w [R]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(R)").eq(1);
        };
        act.click(actionTarget);
    },
    "40.Sprawdzenie wyświetlania listy firm na R": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '41.Kliknięcie w [S]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(S)").eq(1);
        };
        act.click(actionTarget);
    },
    "42.Sprawdzenie wyświetlania listy firm na S": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '43.Kliknięcie w [T]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(T)").eq(0);
        };
        act.click(actionTarget);
    },
    "44.Sprawdzenie wyświetlania listy firm na T": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '45.Kliknięcie w [U]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(U)");
        };
        act.click(actionTarget);
    },
    "46.Sprawdzenie wyświetlania listy firm na U": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '47.Kliknięcie w [V]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(V)");
        };
        act.click(actionTarget);
    },
    "48.Sprawdzenie wyświetlania listy firm na V": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '49.Kliknięcie w [W]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(W)").eq(0);
        };
        act.click(actionTarget);
    },
    "50.Sprawdzenie wyświetlania listy firm na W": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '51.Kliknięcie w [X]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(X)");
        };
        act.click(actionTarget);
    },
    "52.Sprawdzenie wyświetlania listy firm na X": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '53.Kliknięcie w [Y]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(Y)");
        };
        act.click(actionTarget);
    },
    "54.Sprawdzenie wyświetlania listy firm na Y": function() {
        notEq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '55.Kliknięcie w [Z]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(Z)");
        };
        act.click(actionTarget);
    },
    "56.Sprawdzenie wyświetlania listy firm na Z": function() {
        eq($("#content-list > div > company-list-component > div > section > div.ng-isolate-scope > div").children().length, 0);
    },
    '57.Odliknięcie [Z]': function() {
        var actionTarget = function() {
            return $("#content-list").find(":containsExcludeChildren(Z)");
        };
        act.click(actionTarget);
    },
    "58.Włączenie mapy": function() {
        var actionTarget = function() {
            return $(".btn-map").find(" > img:nth(0)");
        };
        act.click(actionTarget);
    },
    "59.Oczekiwanie na załadowanie": function() {
        act.wait(2e3);
    },
    "60.Sprawdzenie widoczności mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "61.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    "62.Wyłączenie mapy": function() {
        act.click(".map-close.btn");
    },
    "63.Sprawdzenie widoczności mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_003WyświetlanieOgłoszenia"] = {
    '1.Click span "Senior .Net..."': function() {
        act.click(":containsExcludeChildren(Senior Net Developer)");
    },
    "2.Sprawdzenie wyświetlania kategorii": function() {
        ok($(".col-flex.remove-padding").find(" > h1:nth(0)").length > 0);
        ok($(":containsExcludeChildren(REQUIREMENTS)").length > 0);
        ok($(":containsExcludeChildren(MUST)").length > 0);
        ok($(":containsExcludeChildren(NICETOHAVE)").length > 0);
        ok($(":containsExcludeChildren(LANGUAGES)").length > 0);
        ok($(":containsExcludeChildren(WORK METHODOLOGY)").length > 0);
        ok($(":containsExcludeChildren(JOB PROFILE)").length > 0);
        ok($(":containsExcludeChildren(EQUIPMENT SUPPLIED)").length > 0);
        ok($(":containsExcludeChildren(SPECS)").length > 0);
        ok($(":containsExcludeChildren(ESSENTIALS)").length > 0);
        ok($(":containsExcludeChildren(ONEONONE)").length > 0);
        ok($(":containsExcludeChildren(this and that)").length > 0);
        ok($(":containsExcludeChildren(OTHER INFO)").length > 0);
        ok($(":containsExcludeChildren(TECHNOLOGIES USED)").length > 0);
        ok($(":containsExcludeChildren(Apply)").length > 0);
    },
    "3.Włączenie mapy": function() {
        var actionTarget = function() {
            return $(".btn-map").find(" > img:nth(0)");
        };
        act.click(actionTarget);
    },
    "4.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "5.Wyłączenie mapy": function() {
        act.click(".map-close.btn");
    },
    "4.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_004WyświetlanieProfiluFirmy"] = {
    "2.Oczekiwanie na załadowanie": function() {
        act.wait(2e3);
    },
    '1.Kliknięcie w "Companies"': function() {
        act.click("body > div:nth(1) > header:nth(0) > div:nth(0) > div:nth(0) > div:nth(0) > a:nth(1) > span:nth(0)");
    },
    "2.Kliknięcie w firmę z listy": function() {
        var actionTarget = function() {
            return $(".company-item.pin-animate").eq(0);
        };
        act.click(actionTarget);
    },
    "3.Sprawdzenie wyświetlania kategorii": function() {
        ok($(".row").find(" > h1:nth(0)").eq(0).length > 0);
        ok($(":containsExcludeChildren(About company)").length > 0);
        ok($("#technologies-and-benefits").find(":containsExcludeChildren(Technologies)").length > 0);
        ok($("#technologies-and-benefits").find(":containsExcludeChildren(Benefits)").length > 0);
    },
    "4.Włączenie mapy": function() {
        act.click(":containsExcludeChildren(SHOW MAP)");
    },
    "1.Wait 1000 milliseconds": function() {
        act.wait(3e3);
    },
    "5.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "visible");
    },
    "6.Wyłączenie mapy": function() {
        act.click(":containsExcludeChildren(CLOSE)");
    },
    "5.Sprawdzenie wyświetlania mapy": function() {
        eq($(".gm-style").find(" > div:nth(0) > div:nth(2)").css("visibility"), "hidden");
    }
};

"@test"["ST_005PorównywanieOfert"] = {
    "1.Hover over link": function() {
        act.hover("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(0) > a:nth(0)");
    },
    "2.Click span": function() {
        act.click("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(0) > a:nth(0) > div:nth(1) > button:nth(0) > span:nth(0)");
    },
    "3.Hover over link": function() {
        act.hover("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > a:nth(0)");
    },
    "4.Click span": function() {
        act.click("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > a:nth(0) > div:nth(1) > button:nth(0) > span:nth(0)");
    },
    "5.Hover over link": function() {
        act.hover("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(2) > a:nth(0)");
    },
    "6.Click span": function() {
        act.click("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(2) > a:nth(0) > div:nth(1) > button:nth(0) > span:nth(0)");
    },
    "7.Hover over div": function() {
        var actionTarget = function() {
            return $(".panel-body").eq(1);
        };
        act.hover(actionTarget);
    },
    "8.Click span": function() {
        act.click(".glyphicon.glyphicon-shopping-cart");
    },
    "9.Assert": function() {
        ok($("#container").find(".panel-body.mobile-top").eq(3).length > 0);
        ok($("#container").find(".panel-body.mobile-top").eq(4).length > 0);
        ok($("#container").find(".panel-body.mobile-top").eq(5).length > 0);
        ok($("#container").find(".panel-body").eq(7).length > 0);
        ok($("#container").find(".panel-body").eq(3).length > 0);
        ok($("#container").find(".panel-body").eq(11).length > 0);
        ok($("#container").find(".panel-body").eq(15).length > 0);
        ok($("#container").find(".panel-body").eq(19).length > 0);
        ok($("#container").find(".panel-body").eq(23).length > 0);
        ok($("#container").find(".panel-body").eq(27).length > 0);
        ok($("#container").find(".panel-body").eq(31).length > 0);
        ok($("#container").find(".panel-body").eq(35).length > 0);
        ok($("#container").find(".panel-body").eq(39).length > 0);
        ok($("#container").find(".panel-body").eq(43).length > 0);
        ok($("#container").find(".panel-body").eq(47).length > 0);
        ok($("#container").find(".panel-body").eq(51).length > 0);
        ok($("#container").find(".panel-body").eq(55).length > 0);
        ok($("#container").find(".panel-body").eq(59).length > 0);
        ok($("#container").find(".panel-body").eq(62).length > 0);
        ok($(".panel-body").eq(1).length > 0);
        ok($(".panel-body").eq(2).length > 0);
        ok($(".panel-body").eq(3).length > 0);
        ok($(".panel-body").eq(4).length > 0);
        ok($(".panel-body").eq(5).length > 0);
        ok($(".panel-body").eq(6).length > 0);
        ok($(".panel-body").eq(7).length > 0);
        ok($(".panel-body").eq(8).length > 0);
        ok($(".panel-body").eq(9).length > 0);
        ok($(".panel-body").eq(10).length > 0);
    },
    "10.Assert": function() {
        eq($("body > div:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > table:nth(0) > tbody:nth(0) > tr:nth(1)  ").children().length, 4);
    },
    "11.Hover over div": function() {
        var actionTarget = function() {
            return $(".compare-widget.panel").find(" > div:nth(1)");
        };
        act.hover(actionTarget);
    },
    "12.Click span": function() {
        var actionTarget = function() {
            return $(".glyphicon.glyphicon-remove.compare-widget-remove.nfj-red.ng-scope").eq(0);
        };
        act.click(actionTarget);
    },
    "13.Assert": function() {
        eq($("body > div:nth(0) > section:nth(0) > div:nth(0) > div:nth(1) > table:nth(0) > tbody:nth(0) > tr:nth(1)  ").children().length, 3);
    }
};


"@test"["ST_007PrzesuwanieMapki"] = {
    '1.Cookie accept "OK, I got it"': function() {
        act.click(":containsExcludeChildren(OK I got it)");
    },
    '2.Kliknięcie w ofertę "Software (Python /..."': function() {
        act.click("body > div:nth(1) > section:nth(0) > div:nth(2) > div:nth(0) > div:nth(0) > postings-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(9) > a:nth(0) > h3:nth(0) > span:nth(0)");
    },
    "3.Włączenie mapki": function() {
        act.click(":containsExcludeChildren(SHOW MAP)");
    },
    "4.Kliknięcie w search": function() {
        act.click(".header-wrapper");
    },
    "5.Sprawdzenine pozycji na mapie": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(4) > div > div").attr("style"), "display: block; position: absolute; left: -16.0288px; top: -64.4816px;");
    },
    "6.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    '7.Przejście do nastepnej oferty': function() {
        act.click("body > div:nth(1) > section:nth(0) > div:nth(0) > div:nth(0) > div:nth(0) > posting-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(0) > div:nth(0) > article-header:nth(0) > div:nth(0) > div:nth(1) > link-next:nth(0) > a:nth(0) > span:nth(0)");
    },
    "8.Sprawdzenine pozycji na mapie": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(4) > div > div").attr("style"), "display: block; left: -15.7781px; top: -63.6581px; position: absolute;");
    },
    "9.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    '10.Przejście do nastepnej oferty': function() {
        act.click("body > div:nth(1) > section:nth(0) > div:nth(0) > div:nth(0) > div:nth(0) > posting-component:nth(0) > section:nth(0) > div:nth(0) > div:nth(0) > div:nth(0) > article-header:nth(0) > div:nth(0) > div:nth(1) > link-next:nth(0) > a:nth(0) > span:nth(0)");
    },
    "11.Sprawdzenine pozycji na mapie": function() {
        eq($("#map > nfj-map > div > ng-map > div > div > div > div:nth-child(1) > div:nth-child(3) > div > div:nth-child(4) > div > div").attr("style"), "display: block; left: -15.53px; top: -64.248px; position: absolute;");
    },
    "12.Zrobienie screenshotu": function() {
        act.screenshot();
    },
    "13.Wyłączenine mapki": function() {
        act.click(":containsExcludeChildren(CLOSE)");
    }
};


"@test"["ST_009Tagowanie"] = {
    "1.Wpisanie filtru ''warsaw''": function() {
        act.type(".ng-pristine.ng-untouched.ng-valid.ng-empty", "warsaw");
    },
    "2.Wciśnięcie ENTER": function() {
        act.press("enter");
    },
    "3.Sprawdzeine wyświetlania filtru ''city=warszawa''": function() {
        eq($(".span_item.tags.ng-isolate-scope").find(" > span:nth(0)").text(), "city=warszawa");
    },
    "4.Wpisannie filtru ''lodz''": function() {
        act.type(".ng-untouched.ng-valid.ng-dirty.ng-empty", "lodz");
    },
    "5.Wciśnnięcie ENTER": function() {
        act.press("enter");
    },
    "6.Sprawdzenie wyświetlaina filtru ''city=warszawa,lodz''": function() {
        eq($(".span_item.tags.ng-isolate-scope").find(" > span:nth(0)").text(), "city=warszawa,lodz");
    },
    "7.Wciśnięcie przycisku filtrów": function() {
        var actionTarget = function() {
            return $("#header").find(".menu-button");
        };
        act.click(actionTarget);
    },
    '8.Kliknięcie "python"': function() {
        var actionTarget = function() {
            return $(":containsExcludeChildren(python)").eq(0);
        };
        act.click(actionTarget);
    },
    '9.Kliknięcie "android"': function() {
        var actionTarget = function() {
            return $(":containsExcludeChildren(android)").eq(0);
        };
        act.click(actionTarget);
    },
    '10.Kliknięcie "Close"': function() {
        act.click(":containsExcludeChildren(Close)");
    },
    "11.Sprawdzenie wyświetlania Android i Python": function() {
        eq($("#header").find(".ng-binding").eq(1).text(), "android");
        eq($("#header").find(".ng-binding").eq(0).text(), "python");
    }
};